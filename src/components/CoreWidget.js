/** Do not rename this file **/
import React from 'react';

export default class scratchpadWidget extends React.Component {
  static propTypes = {
    width: React.PropTypes.number,
    height: React.PropTypes.number,
    settings: React.PropTypes.object.isRequired,
  };

  static id = 'scratchpad';
  static widgetName = 'your-widget-name';
  static sizes = [[2, 2]];

  enbiggen() {
      // Happens on blur.

      const text = document.getElementById("scratchpad").value;

      const temp = this.props.settings.get("styles", false);
      var copy = jQuery.extend({}, temp)
      copy["fontSize"] = "400%";
      copy["textAlign"] = "center";
      copy["verticalAlign"] = "middle";
      copy["paddingTop"] = "30%";
      copy["lineHeight"] = "120%";
      this.props.settings.set("styles", copy);
  }

  clear() {
      const temp = this.props.settings.get("styles", false);
      var copy = jQuery.extend({}, temp)
      copy["fontSize"] = "100%";
      copy["textAlign"] = "left";
      copy["paddingTop"] = "2%";
      copy["lineHeight"] = "100%";
      this.props.settings.set("styles", copy);
  }

  componentWillMount() {
      const styles = { maxHeight: "100%", height: "100%", width: "100%", display: "table-row", resize: "none", fontSize: "100%"};
      this.props.settings.set("styles", styles);
  }

  render() {
    const styles = this.props.settings.get("styles", false);

    return (
      <div className="uk-margin uk-form" style={{ height: "100%" }}>
        <textarea style={ styles }
                  placeholder="Enter text here." onBlur={this.enbiggen.bind(this)}
                  className="uk-text-large uk-text-bold" id="scratchpad"
                  onFocus={this.clear.bind(this)}></textarea>
      </div>
    );
  }
}
